diff = function (a1, a2) {
    var a = [], diff = [];
    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = a1;
    }
    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }
    for (var k in a) {
        diff.push(k);
    }
    return diff;
}

$(function () {

    function base64Encode(stringInput) {

        // NOTE: This normalization technique for handling characters that require
        // more than an 8-bit representation was provided on the Mozilla Developer
        // Network website for Base64 encoding. They also provided a companion DECODE
        // method. But, we don't need to decode in this demo.
        // --
        // READ MORE: https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding#The_Unicode_Problem
        var normalizedInput = encodeURIComponent(stringInput).replace(
            /%([0-9A-F]{2})/g,
            function toSolidBytes($0, hex) {

                return (String.fromCharCode("0x" + hex));

            }
        );

        return (btoa(normalizedInput));

    }

    function b64DecodeUnicode(str) {
        // Going backwards: from bytestream, to percent-encoding, to original string.
        return decodeURIComponent(atob(str).split('').map(function (c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    }

    /*
    var resize;
    function headerSize(event) {
        let $this = $(this);
        resize && clearInterval(resize);
        resize = setTimeout(function () {
            $('content').animate({ marginTop: $this.closest('header').height() + 25 });
            console.log($this.closest('header').height());
        }, 50);
    }

    $(window).on('resize', headerSize.bind($('header'))).trigger('resize');
    */

    $('header .codes, header .breaks').on('DOMNodeInserted', '[data-target=".codes"],[data-target=".breaks"]', function (event) {
        let $this = $(this);
        //headerSize.apply(this, event);
        $this.draggable({
            appendTo: 'body',
            cursor: 'move',
            helper: "clone",
            start: function(event, ui){
                $(this).draggable('instance').offset.click = {
                    left: Math.floor(ui.helper.width() / 2),
                    top: Math.floor(ui.helper.height() / 2)
                }; 
            },
            revert: 'invalid'
        })
    });

    $('[name=newcode]').on('keydown', function (event) {
        if (event.which == 13) {
            var val = '[[' + $(this).val() + ']]';
            if (!$('header ' + $(this).data('target') + ' [data-target]').get().filter(function (item) {
                return $(item).data('id') == val
            }).length) {
                var $span = $('<span/>');
                $span.attr('data-id', val);
                $span.attr('data-target', $(this).data('target'));
                $span.attr('class', 'inductive');
                /* $span.text('\n' + val + '\n'); */
                $span.text(' ' + val + '');
                $('header ' + $(this).data('target')).append($span);
            }
            $(this).val('');
        }
    })

    $('[name=newbreak]').on('keydown', function (event) {
        if (event.which == 13) {
            var val = '---<<' + $(this).val() + '>>---';
            if (!$('header ' + $(this).data('target') + ' [data-target]').get().filter(function (item) {
                return $(item).data('id') == val
            }).length) {
                var $span = $('<span/>');
                $span.attr('data-id', val);
                $span.attr('data-target', $(this).data('target'));
                $span.attr('class', 'inductive');
                $span.text('\n' + val + '\n');
                /* $span.text(' ' + val + '\n'); */
                $('header ' + $(this).data('target')).append($span);
            }
            $(this).val('');
        }
    })


    $("content").on('DOMNodeInserted', '.line', function (event) {
        $(this).droppable({
            accept: function ($this) {
                return !$(this).find($this.data('target')).has('[data-id="' + String($this.data('id') || "").replace(/"/, '\\\\"') + '"]').length;
            },
            drop: function (event, ui) {
                var $this = ui.draggable;
                var $thesecodes = $(this).find($this.data('target'));
                $this.clone().appendTo($thesecodes).one('click', function () {
                    $(this).remove();
                });
            }
        });


    });

    $('.download').on('click', function () {

        var unused = diff($('content [data-target=".codes"].inductive, content [data-target=".breaks"].inductive').get().map(item => $(item).data('id')), $('header .inductive').get().map(item => $(item).data('id')));

        if (unused.length) {
            var answer = confirm('You haven\'t used the following inductive codes. Is that OK?\n\n- ' + unused.join('\n- '));

            if (!answer) return false;
        }

        blob = new Blob(
            [$('content').text().replace(/\s*$/gm, "")],
            {
                type: "text/plain;charset=utf-8"
            }
        );
        $(this).attr('href', URL.createObjectURL(blob));
    });

    $('input[type=file]').on('change', function () {
        if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
            alert('The File APIs are not fully supported in this browser.');
            return;
        }

        input = $(this).get(0);
        if (!input) {
            alert("Um, couldn't find the fileinput element.");
        }
        else if (!input.files) {
            alert("This browser doesn't seem to support the `files` property of file inputs.");
        }
        else if (!input.files[0]) {
            alert("Please select a file before clicking 'Load'");
        }
        else {

            file = input.files[0];
            fr = new FileReader();

            $this = $(this);
            $target = $($this.attr('name'));

            fr.onload = function () {
                var data = b64DecodeUnicode(this.result.split(',')[1]);
                // console.log($my, data);
                data = data.replace(/\s*$/gm, "").split(/\r?\n/g).map(function (i) {
                    var $line = $($this.data('template'));

                    if ($this.hasClass('file')) {
                        $('.download').attr('download', $this.val().replace(/^.*[\/\\](.*?)$/g, '$1')).removeClass('disabled');
                    }

                    if ($this.data('target')) {
                        if (i.length == 0) {
                            $line.find($this.data('target')).html("&nbsp;");
                        } else {
                            $line.find($this.data('target')).text(i);
                        }
                    }

                    if ($this.data('id')) {
                        $line.attr($this.data('id'), i);
                        if ($this.hasClass('codes'))
                            $line.text(' ' + i);
                        else
                            $line.text("\n" + i);
                    }

                    var html = $('<div/>').append($line).html() + "\n";
                    //console.log(html);  
                    return html;
                }).join('');
                //console.log(data);
                $target.html(data);
            };
            //fr.readAsText(file);
            fr.readAsDataURL(file);

            //for (i=0;i<100;i++) $('header').append('<span data-id="'+i+'" data-target=".codes"></span>')
        }
    });
});
