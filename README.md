# iROCK

iROCK is an interface for the Reproducible Open Coding Kit (see https://rockbook.org).

This is the GitLab repository. You can download the application to your
computer and run it locally or run this version, which is hosted at https://i.rock.science.

